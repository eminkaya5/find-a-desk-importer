# Import Find a Desk

Excel desk data importer for desk-share
Application requests to Desk-Share-API to import the desks from the .xlsx file in folder

Structur of location (example):

|              | Location | Site     | Building | Floor      | Room          |
|--------------|----------|----------|----------|------------|---------------|
| id           | Weissach | GER08    | GER0852  | GER0852OG2 | GER0852OG2206 |
| name         | Weissach | GER08    | GER0852  | GER0852OG2 | GER0852OG2206 |
| pid          | null     | Weissach | GER08    | GER0852    | GER0852OG2    |
| locationType | LOCATION | SITE     | BUILDING | FLOOR      | ROOM          |



Structur of organizations (example):

|                  | Company | Ressort | Department | Organizational Unit |
|------------------|---------|---------|------------|---------------------|
| id               | PORSCHE | B       | BA         | BAB                 |
| name             | PORSCHE | B       | BA         | BAB                 |
| pid              | null    | B       | B          | BA                  |
| organizationTYpe | COMPANY | RESSORT | DEPARTMENT | ORGANIZATIONAL_UNIT |

Structur of desks (example):

|                    | Einzel   | Sharing | 
|--------------------|----------|---------|
| id                 | 3763     | 15286   | 
| number             | 2-040    | 0-033   | 
| deskStatus         | ENABLED  | DISABLED       | 
| locationModelId    | GER0852OG2206 | GER04M74EG004 |
| organizationUnitId | BC       | FIP |
| locationModelId    | MONDAY=false, <br/>TUESDAY=false, <br/>WEDNESDAY=false, <br/>THURSDAY=false, <br/>FRIDAY=false| MONDAY=true,<br/> TUESDAY=true,<br/> WEDNESDAY=true,<br/> THURSDAY=true,<br/> FRIDAY=true|

Structur of organization admin (example):

|                    | Organization Admin                   | 
|--------------------|--------------------------------------|
| adminId                 | 63df6354-6f7a-428c-9fd3-19f37669035f | 
| organizationUnitId             | B                                    | 