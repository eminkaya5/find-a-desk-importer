package model;

public enum EnumOrganizationType {
    COMPANY, RESSORT, DEPARTMENT, ORGANIZATIONAL_UNIT
}
