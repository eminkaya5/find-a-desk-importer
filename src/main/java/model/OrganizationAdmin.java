package model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrganizationAdmin {
    private String adminId;
    private String organizationUnitId;
}
