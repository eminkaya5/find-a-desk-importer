package model;

public enum EnumDeskStatus {
    ENABLED, DISABLED, ARCHIVED
}
