package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;

@Data
@AllArgsConstructor
public class Organization {
    private String id;
    private String name;
    private String parentId;
    private EnumOrganizationType organizationType;

    public static Comparator<Organization> organizationComparator = Comparator.comparing(Organization::getOrganizationType);
}
