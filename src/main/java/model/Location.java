package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;

@Data
@AllArgsConstructor
public class Location {
    private String id;
    private String name;
    private String parentId;
    private EnumLocation locationType;

    public static Comparator<Location> locationComparator = Comparator.comparing(Location::getLocationType);
}
