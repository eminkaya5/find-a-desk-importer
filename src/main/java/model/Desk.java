package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;
import java.util.HashMap;

@Data
@AllArgsConstructor
public class Desk {
    private String id;
    private String number;
    private EnumDeskStatus deskStatus;
    private String locationModelId;
    private String organizationUnitId;
    private HashMap<String, Boolean> freeDaysForBooking;

    public static Comparator<Desk> deskComparator = (desk1, desk2) -> {

        int idDesk1 = Integer.parseInt(desk1.getId());
        int idDesk2 = Integer.parseInt(desk2.getId());

        return idDesk1 - idDesk2;

    };
}
