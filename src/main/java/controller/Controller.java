package controller;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ImportService;

import java.io.IOException;

@AllArgsConstructor
public class Controller {
    private ImportService importService;

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());


    public void importLocations() {
        try {
            importService.importLocations();
        } catch (IOException e) {
            logger.error("Could not import locations: " + e.getMessage());
        }
    }

    public void importOrganizations() {
        try {
            importService.importOrganizations();
        } catch (IOException e) {
            logger.error("Could not import ...: " + e.getMessage());
        }
    }

    public void importDesks() {
        try {
            importService.importDesks();
        } catch (IOException e) {
            logger.error("Could not import ...: " + e.getMessage());
        }
    }

    public void importOrganizationAdmin() {
        try {
            importService.importOrganizationAdmin();
        } catch (IOException e) {
            logger.error("Could not import ...: " + e.getMessage());
        }
    }
}
