import controller.Controller;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.injectors.ConstructorInjection;
import repository.DeskRepository;
import repository.LocationRepository;
import repository.OrganizationAdminRepository;
import repository.OrganizationRepository;
import service.Constans;
import service.ImportService;
import service.ParserService;

public class Main {
    private static final MutablePicoContainer container = new DefaultPicoContainer(new ConstructorInjection());

    public static void main(String[] args) {
        registerComponents();
       // container.getComponent(Controller.class).importLocations();
        container.getComponent(Controller.class).importOrganizations();
    //    container.getComponent(Controller.class).importDesks();
      //  container.getComponent(Controller.class).importOrganizationAdmin();
    }

    private static void registerComponents() {
        container.addComponent(Controller.class);
        container.addComponent(DeskRepository.class);
        container.addComponent(OrganizationRepository.class);
        container.addComponent(LocationRepository.class);
        container.addComponent(OrganizationAdminRepository.class);
        container.addComponent(Constans.class);
        container.addComponent(ImportService.class);
        container.addComponent(ParserService.class);
    }
}

