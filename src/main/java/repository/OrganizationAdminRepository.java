package repository;

import lombok.RequiredArgsConstructor;
import model.OrganizationAdmin;
import service.ParserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class OrganizationAdminRepository {
    private final ParserService parserService;
    public List<OrganizationAdmin> organizationAdminList;

    public List<OrganizationAdmin> getAllOrganizationAdminList() throws IOException {
        organizationAdminList = new ArrayList<>();
        parserService.parseOrganizationAdmin(organizationAdminList);
        return organizationAdminList;
    }
}
