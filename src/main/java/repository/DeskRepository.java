package repository;

import lombok.RequiredArgsConstructor;
import model.Desk;
import service.ParserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
public class DeskRepository {
    private final ParserService parserService;
    public List<Desk> deskList;

    public List<Desk> getAllDesks() throws IOException {
        deskList = new ArrayList<>();
        parserService.parseDesk(deskList);
        deskList.sort(Desk.deskComparator);

        return deskList;
    }
}
