package repository;

import lombok.RequiredArgsConstructor;
import model.Location;
import service.ParserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class LocationRepository {
    private final ParserService parserService;
    public List<Location> locationList;

    public List<Location> getAllLocations() throws IOException {
        locationList = new ArrayList<>();
        parserService.parseLocation(locationList);
        locationList.sort(Location.locationComparator);

        return locationList;
    }
}
