package repository;

import lombok.RequiredArgsConstructor;
import model.EnumOrganizationType;
import model.Organization;
import service.ParserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class OrganizationRepository {
    private final ParserService parserService;
    public List<Organization> organizationList;

    public List<Organization> getALlOrganization() throws IOException {
        organizationList = new ArrayList<>();
        parserService.parseOrganization(organizationList);
        organizationList.add(new Organization("Porsche", "Porsche", null, EnumOrganizationType.COMPANY));
        organizationList.sort(Organization.organizationComparator);
        return organizationList;
    }
}
