package service;

import lombok.RequiredArgsConstructor;
import model.Desk;
import model.Location;
import model.Organization;
import model.OrganizationAdmin;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repository.DeskRepository;
import repository.LocationRepository;
import repository.OrganizationAdminRepository;
import repository.OrganizationRepository;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static service.Constans.*;

@RequiredArgsConstructor
public class ImportService {
    private final OrganizationRepository organizationRepository;
    private final OrganizationAdminRepository organizationAdminRepository;
    private final DeskRepository deskRepository;
    private final LocationRepository locationRepository;

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());
    private int updatedItems = 0;
    private int createdItems = 0;
    private int errorItems = 0;
    private String errors = "\nID\t\tMessage\n";

    public void importLocations() throws IOException {
        logger.info("Start importing locations:");
        List<Location> locationList = locationRepository.getAllLocations();
        for (Location location : locationList) {
            JSONObject jsonObject = new JSONObject(location);
            createPutRequest(jsonObject.toString(), requestLocationPath);
        }
        logger.info("Locations:");
        createLogger();
        logger.info("Finished importing locations:");
    }

    public void importOrganizations() throws IOException {
        logger.info("Start importing organizations:");
        List<Organization> organizationList = organizationRepository.getALlOrganization();
        for (Organization organization : organizationList) {
            JSONObject jsonObject = new JSONObject(organization);
            createPutRequest(jsonObject.toString(), requestOrganizationPath);
        }
        logger.info("Organizations:");
        createLogger();
        logger.info("Finished importing organizations:");
    }

    public void importDesks() throws IOException {
        logger.info("Start importing desks:");
        List<Desk> deskList = deskRepository.getAllDesks();
        JSONArray jsonArray = new JSONArray(deskList);
        createPutRequest(jsonArray.toString(), requestDeskPath);

        logger.info("Desks:");
        createLogger();
        logger.info("Finished importing desks:");
    }

    public void importOrganizationAdmin() throws IOException {
        logger.info("Start importing organization admins:");
        List<OrganizationAdmin> organizationAdminList = organizationAdminRepository.getAllOrganizationAdminList();
        for (OrganizationAdmin organizationAdmin : organizationAdminList) {
            JSONObject jsonObject = new JSONObject(organizationAdmin);
            createPutRequest(jsonObject.toString(), requestOrganizationAdminsPath);
        }
        logger.info("Organization admins:");
        createLogger();
        logger.info("Finished importing organization admins:");
    }


    private void createLogger() {
        logger.info("TOTAL CREATED UNITS: " + createdItems);
        logger.info("TOTAL UPDATED UNITS: " + updatedItems);
        logger.info("TOTAL ERRORS: " + errorItems);
        logger.info("ERROR MESSAGE: " + errors);
    }

    private void createPutRequest(String body, String requestPath) throws IOException {
        String requestUrl = BASE_URL + requestPath;
        URL url = new URL(requestUrl);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL21odWIub25lIiwiZ3JvdXBzIjpbInN1cGVyLWFkbWluIl0sIm9pZCI6ImJvYiIsImlhdCI6MTYzOTU2NDkwM30.bvfkwcr5qPte4l85ld0Iud95Nc7eH1GM-3ygwDftLj0");
        connection.setConnectTimeout(5000);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");

        OutputStream os = connection.getOutputStream();
        os.write(body.getBytes(StandardCharsets.UTF_8));
        os.close();

        InputStream in = new BufferedInputStream(connection.getInputStream()); // variablen name
        String result = IOUtils.toString(in, StandardCharsets.UTF_8);
        connection.disconnect();


        JSONObject jsonObject = new JSONObject(result);
        logItemsFromResponse(jsonObject);

        logger.info(connection.getResponseCode() + "\n" + jsonObject.toString(3));

    }

    private void logItemsFromResponse(JSONObject jsonObject) {
        updatedItems += (int) jsonObject.get("updatedItems");
        createdItems += (int) jsonObject.get("createdItems");
        JSONArray jsonArray = jsonObject.getJSONArray("errorItems");
        if (jsonArray.length() >= 1) {
            for (int i = 0; i < jsonArray.length(); i++) {
                errors += jsonArray.getJSONObject(i).getString("id") + "\t" + jsonArray.getJSONObject(i).getString("message") + "\n";
            }
            errorItems += jsonArray.length();
        }
    }
}
