package service;

import model.Desk;
import model.Location;
import model.Organization;
import model.OrganizationAdmin;
import org.apache.logging.log4j.Logger;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static model.EnumDeskStatus.DISABLED;
import static model.EnumDeskStatus.ENABLED;
import static model.EnumLocation.*;
import static model.EnumOrganizationType.*;

public class ParserService {
    private FileInputStream file = null;
    private Workbook workbook;
    private Sheet sheet;
    private ArrayList<String> idList = new ArrayList<>();
    private Set<String> organizationUnitIdList = new LinkedHashSet<>();
    private HashMap<String, String> locationCacheMap = new LinkedHashMap<>();
    private List<Organization> organizationCacheList = new LinkedList<>();
    private Logger logger;


    private String[] fileArray = {"./GER-4-90-Arbeitsplätze(70).xlsx", "./GER-4-M71-Arbeitsplätze.xls", "./GER-4-M72-Arbeitsplätze.xls", "./GER-4-M73-Arbeitsplätze.xls", "./GER-4-M74-Arbeitsplätze.xls", "./Arbeitsplätze_B-Ressort(94).xlsx", "./Arbeitsplätze_VI.xls"};

    public void parseLocation(List<Location> locationList) throws IOException {
        try {
            parseMultipleLocation(locationList, 4, "./20211025_PAG-Immobilien+Mietobjekte_Übersicht.xlsx");
            for (String file : fileArray) {
                parseMultipleLocation(locationList, 8, file);
            }
        } catch (FileNotFoundException | OfficeXmlFileException e) {
            e.printStackTrace();
        }
    }

    public void parseOrganization(List<Organization> organizationList) throws IOException {
        Organization organization;

        try {
            for (String files : fileArray) {
                file = new FileInputStream(files);
                workbook = new XSSFWorkbook(file);

                sheet = workbook.getSheetAt(0);
                for (Row row : sheet) {
                    organization = new Organization("Porsche", "Porsche", null, COMPANY);

                    if (row.getRowNum() <= 8) {
                        continue;
                    }
                    for (Cell cell : row) {
                        if (cell.getCellType() == CellType.STRING) {
                            String cellValue = cell.getRichStringCellValue().getString();
                            if (cell.getColumnIndex() == 4) {
                                createOrganization(cellValue, organization);
                                splitOrganizationToUnit(organization);
                            }
                        }
                    }
                }
            }

        } catch (FileNotFoundException | OfficeXmlFileException e) {
            logger.error("File could not be found " + e.getMessage());
        }

        for (Organization org : this.organizationCacheList) {
            if (!idList.contains(org.getId())) {
                organizationList.add(org);
                idList.add(org.getId());
            }
        }
    }

    public void parseDesk(List<Desk> deskList) throws IOException {
        Desk desk;
        try {
            for (String files : fileArray) {
                file = new FileInputStream(files);
                workbook = new XSSFWorkbook(file);

                sheet = workbook.getSheetAt(0);

                for (Row row : sheet) {
                    desk = new Desk(null, null, null, null, null, null);

                    if (row.getRowNum() <= 8) {
                        continue;
                    }
                    for (Cell cell : row) {
                        if (cell.getCellType() == CellType.STRING) {
                            String cellValue = cell.getRichStringCellValue().getString();
                            createDesk(cellValue, cell.getColumnIndex(), row, desk);
                        }
                    }
                    deskList.add(desk);
                }
            }

        } catch (FileNotFoundException | OfficeXmlFileException e) {
            logger.error("File could not be found " + e.getMessage());
        }

    }

    public void parseOrganizationAdmin(List<OrganizationAdmin> organizationAdminList) throws IOException {
        OrganizationAdmin organizationAdmin;

        try {
            file = new FileInputStream("./Prod_AD-Groups_IDs.xlsx");
            workbook = new XSSFWorkbook(file);

            sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                organizationAdmin = new OrganizationAdmin(null, null);

                if (row.getRowNum() == 0) {
                    continue;
                }
                for (Cell cell : row) {
                    if (cell.getCellType() == CellType.STRING) {
                        String cellValue = cell.getRichStringCellValue().getString();
                        createOrganizationAdmin(cellValue, cell.getColumnIndex(), organizationAdmin);
                    }
                }
                if (!idList.contains(organizationAdmin.getAdminId())) {
                    if (organizationAdmin.getOrganizationUnitId() != null) {
                        organizationAdminList.add(organizationAdmin);
                        idList.add(organizationAdmin.getAdminId());
                    }
                }
            }


        } catch (FileNotFoundException | OfficeXmlFileException e) {
            logger.error("File could not be found " + e.getMessage());
        }

    }

    private void parseMultipleLocation(List<Location> locationList, int rowComparison, String files) throws IOException {
        Location location = null;

        file = new FileInputStream(files);
        workbook = new XSSFWorkbook(file);
        sheet = workbook.getSheetAt(0);
        for (Row row : sheet) {

            if (row.getRowNum() <= rowComparison) {
                continue;
            }
            for (Cell cell : row) {
                if (cell.getCellType() == CellType.STRING) {
                    String cellValue = cell.getRichStringCellValue().getString();
                    if (rowComparison == 4) {
                        location = createLocationSite(cellValue, cell.getColumnIndex(), row, location);
                    } else if (rowComparison == 8) {
                        location = createBuildingFloorRoom(cellValue, cell.getColumnIndex(), row, location);
                    }
                }

                if (location == null) continue;

                if (!idList.contains(location.getId())) {
                    locationList.add(location);
                    idList.add(location.getId());
                }
            }
        }
    }

    private void createDesk(String cellValue, int columnIndex, Row row, Desk desk) {
        HashMap<String, Boolean> freeDaysForBookingMap = new LinkedHashMap<>();
        int locationColumn = 2;
        int numeberColumn = 3;
        int organizationUnitIdColumn = 4;
        int typColumn = 5;
        int idColumn = 6;
        int statusColumn = 7;


        if (columnIndex == locationColumn) {
            String site = row.getCell(0).getRichStringCellValue().getString();
            String value = row.getCell(1).getRichStringCellValue().getString();
            desk.setLocationModelId(site + value + cellValue);
        } else if (columnIndex == numeberColumn) {
            desk.setNumber(cellValue);
        } else if (columnIndex == organizationUnitIdColumn) {
            desk.setOrganizationUnitId(cellValue);
            if (!desk.getOrganizationUnitId().equals("KEINE")) {
                organizationUnitIdList.add(desk.getOrganizationUnitId().substring(0, 1));
            }
        } else if (columnIndex == typColumn) {
            if (cellValue.equals("Shareing*")) {
                freeDaysForBookingMap.put("MONDAY", true);
                freeDaysForBookingMap.put("TUESDAY", true);
                freeDaysForBookingMap.put("WEDNESDAY", true);
                freeDaysForBookingMap.put("THURSDAY", true);
                freeDaysForBookingMap.put("FRIDAY", true);

                desk.setFreeDaysForBooking(freeDaysForBookingMap);
            } else {
                freeDaysForBookingMap.put("MONDAY", false);
                freeDaysForBookingMap.put("TUESDAY", false);
                freeDaysForBookingMap.put("WEDNESDAY", false);
                freeDaysForBookingMap.put("THURSDAY", false);
                freeDaysForBookingMap.put("FRIDAY", false);
                desk.setFreeDaysForBooking(freeDaysForBookingMap);
            }
        } else if (columnIndex == idColumn) {
            desk.setId(cellValue);
        } else if (columnIndex == statusColumn) {
            if (cellValue.equals("aktiv") || cellValue.equals("wie gerade im System")) {
                desk.setDeskStatus(ENABLED);
            } else if (cellValue.equals("NICHT aktiv")) {
                desk.setDeskStatus(DISABLED);
            }
        }
    }

    private Location createBuildingFloorRoom(String cellValue, int columnIndex, Row row, Location location) {
        int columnBuilding = 0;
        int columnFloor = 0;
        int columnRoom = 0;

        if (columnIndex == columnBuilding) {
            location = new Location("", "", "", null);
            location.setId(cellValue);
            location.setName(cellValue);
            for (String key : locationCacheMap.keySet()) {
                if (key.contains(cellValue)) {
                    location.setParentId(locationCacheMap.get(key));
                }
            }
            location.setLocationType(BUILDING);
        } else if (columnIndex == columnFloor) {
            location = new Location("", "", "", null);
            String cellValuesSite = row.getCell(0).getRichStringCellValue().getString();
            location.setId(cellValuesSite + cellValue);
            location.setName(cellValue);
            location.setParentId(cellValuesSite);
            location.setLocationType(FLOOR);
        } else if (columnIndex == columnRoom) {
            location = new Location("", "", "", null);
            String cellValuesSite = row.getCell(0).getRichStringCellValue().getString();
            String cellValuesFloor = row.getCell(1).getRichStringCellValue().getString();
            location.setId(cellValuesSite + cellValuesFloor + cellValue);
            location.setName(cellValue);
            location.setParentId(cellValuesSite + cellValuesFloor);
            location.setLocationType(ROOM);
        }
        return location;
    }

    private Location createLocationSite(String cellValue, int columnIndex, Row row, Location location) {
        int siteColumn = 1;
        int locationColumn = 7;


        if (columnIndex == siteColumn) {
            location = new Location("", "", "", null);
            location.setId(cellValue);
            location.setName(cellValue);
            String value = cellValue;
            location.setParentId(row.getCell(7).getRichStringCellValue().getString());
            location.setLocationType(SITE);
            cellValue = row.getCell(0).getRichStringCellValue().getString();
            locationCacheMap.put(cellValue, value);
        }

        if (columnIndex == locationColumn) {
            location = new Location("", "", "", null);
            location.setId(cellValue);
            location.setName(cellValue);
            location.setParentId(null);
            location.setLocationType(LOCATION);
        }
        return location;
    }

    private void createOrganizationAdmin(String cellValue, int columnIndex, OrganizationAdmin organizationAdmin) {
        int idColumn = 0;
        int organizationUnitIdColumn = 0;

        if (columnIndex == idColumn) {
            organizationAdmin.setAdminId(cellValue);
        } else if (columnIndex == organizationUnitIdColumn) {
            if (cellValue.contains("_admin_")) {
                organizationAdmin.setOrganizationUnitId(String.valueOf(cellValue.charAt(cellValue.length() - 1)).toUpperCase());
            } else if (cellValue.contains("_superadmin")) {
                organizationAdmin.setOrganizationUnitId("SUPERADMIN");
            }
        }
    }

    private void createOrganization(String cellValue, Organization organization) {
        organization.setId(cellValue);
        organization.setName(cellValue);
        if (cellValue.equals("Porsche")) {
            organization.setId("Porsche");
            organization.setName("Porsche");
            organization.setParentId(null);
            organization.setOrganizationType(COMPANY);
        } else if (cellValue.length() == 1 || cellValue.equals("KEINE")) {
            organization.setParentId("Porsche");
            organization.setOrganizationType(RESSORT);
        } else if (cellValue.length() == 2) {
            organization.setParentId(Character.toString(cellValue.charAt(0)));
            organization.setOrganizationType(DEPARTMENT);
        } else if (cellValue.length() >= 3) {
            organization.setParentId(cellValue.charAt(0) + Character.toString(cellValue.charAt(1)));
            organization.setOrganizationType(ORGANIZATIONAL_UNIT);
        }
    }

    private void splitOrganizationToUnit(Organization organization) {
        Organization cacheOrg = new Organization("", "", "", null);
        organizationCacheList.add(organization);
        if (organization.getOrganizationType().equals(ORGANIZATIONAL_UNIT)) {
            cacheOrg.setId(organization.getId().substring(0, 2));
            cacheOrg.setName(organization.getId().substring(0, 2));
            cacheOrg.setParentId(organization.getId().substring(0, 1));
            cacheOrg.setOrganizationType(DEPARTMENT);
            organizationCacheList.add(cacheOrg);
            splitOrganizationToUnit(cacheOrg);
        } else if (organization.getOrganizationType().equals(DEPARTMENT)) {
            cacheOrg.setId(organization.getId().substring(0, 1));
            cacheOrg.setName(organization.getId().substring(0, 1));
            cacheOrg.setParentId("Porsche");
            organizationCacheList.add(cacheOrg);
            cacheOrg.setOrganizationType(RESSORT);
        }
    }
}